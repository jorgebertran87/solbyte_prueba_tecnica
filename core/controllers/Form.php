<?php
namespace controllers;
include_once 'core/models/Form.php';
/**
 * Created by PhpStorm.
 * User: fani
 * Date: 14/08/15
 * Time: 11:40
 */

class Form {
    public function __construct() {
        $this->model = new \models\Form();
        $this->applyAction($_REQUEST['action']);
        return $this;
    }

    public function draw() {
        include_once 'core/views/form.php';
    }

    private function applyAction($action)  {
        if(method_exists($this->model, $action))
            $this->model->$action($_POST['params']);
    }


}