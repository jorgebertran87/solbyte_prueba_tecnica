<?php
namespace models;
include_once 'core/Database.php';
/**
 * Created by PhpStorm.
 * User: fani
 * Date: 14/08/15
 * Time: 11:40
 */

class Form {
    public function loadMarcas() {
        $db = new \Database('192.168.10.11', 'homestead', 'secret', 'solbyte');
        $regs = $db->query("SELECT * FROM marcas");
        $result = array();
        while($reg = mysql_fetch_assoc($regs)) {
            $result[] = array("id" => $reg['id'], "value" => $reg['marca']);
        }
        echo json_encode($result);
    }

    public function loadModelos($params) {
        $db = new \Database('192.168.10.11', 'homestead', 'secret', 'solbyte');
        $regs = $db->query("SELECT * FROM modelos WHERE marcas_id = '".$params['marcaId']."'");
        $result = array();
        while($reg = mysql_fetch_assoc($regs)) {
            $result[] = array("id" => $reg['id'], "value" => $reg['modelo']);
        }
        echo json_encode($result);
    }

    public function generarCsv($params) {
        $ids = explode('_', $params['ids']);

        $ids_ok = array();
        foreach($ids as $id)
            if($id != "") $ids_ok[] = $id;
        $ids = implode(',', $ids_ok);
        $db = new \Database('192.168.10.11', 'homestead', 'secret', 'solbyte');
        $sql = "SELECT ma.marca, mo.modelo FROM marcas ma INNER JOIN modelos mo ON ma.id = mo.marcas_id AND mo.id IN ($ids)";
        $regs = $db->query($sql);
        $result = array();
        $result[] = array("marca", "modelo");
        while($reg = mysql_fetch_assoc($regs)) {
            $result[] = array($reg['marca'], $reg['modelo']);
        }

        echo json_encode($result);
    }

}