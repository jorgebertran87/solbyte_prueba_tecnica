/**
 * Created by fani on 14/08/15.
 */

jQuery(document).ready( function() {
    Form.loadMarcas();

    jQuery('#marca').change( function() {
        var marca_id = jQuery(this).val();
        Form.loadModelos(marca_id);
    });

    jQuery('#seleccionar').click( function() {
        var modelo_id = jQuery('#modelo').val();
        var modelo = jQuery('#modelo option:selected').text();
        jQuery('#terminalesIds').val( jQuery('#terminalesIds').val() + '_' + modelo_id);
        jQuery('#terminales').val( jQuery('#terminales').val() + ' ' + modelo);
    });

    jQuery('#generarCsv').click( function() {
        var ids = jQuery('#terminalesIds').val();
        Form.generarCsv(ids);
    });
});

var Form =
{
  loadMarcas: function() {
      var data = {
        action: 'loadMarcas'
      };
      jQuery.ajax({
          type: "POST",
          url: "index.php?raw=1",
          dataType: "json",
          data: data,
          success: function (response) {
              for(var i in response) {
                  var marca = response[i];
                  jQuery('#marca').append('<option value="' + marca.id  + '">' + marca.value + '</option>');
              }
          },
          error: function (xhr, ajaxOptions, thrownError) {
              alert(xhr.status);
              alert(thrownError);
          }
      });
  },
  loadModelos: function(marca_id) {
        var data = {
            action: 'loadModelos',
            params: {
                marcaId: marca_id
            }
        };
        jQuery.ajax({
            type: "POST",
            url: "index.php?raw=1",
            dataType: "json",
            data: data,
            success: function (response) {
                jQuery('#modelo option').each( function() {
                   if(jQuery(this).val() > 0)
                       jQuery(this).remove();
                });
                for(var i in response) {
                    var marca = response[i];
                    jQuery('#modelo').append('<option value="' + marca.id  + '">' + marca.value + '</option>');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
  },
  generarCsv: function(ids) {
        var data = {
            action: 'generarCsv',
            params: {
                ids: ids
            }
        };
        jQuery.ajax({
            type: "POST",
            url: "index.php?raw=1",
            data: data,
            dataType: 'json',
            success: function (data) {
                var csvContent = "data:text/csv;charset=utf-8,";
                for(var i in data) {
                    var dataString = data[i].join(",");
                    csvContent += dataString+ '\r\n';
                }
                var encodedUri = encodeURI(csvContent);
                var link = document.createElement("a");
                link.setAttribute("href", encodedUri);
                link.setAttribute("download", "modelos.csv");
                link.click(); // This will download the data file named "my_data.csv".
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
  }
};
