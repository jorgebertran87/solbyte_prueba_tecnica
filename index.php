<?php
    include_once 'core/controllers/Form.php';
    $form = new \controllers\Form();
    if(!isset($_REQUEST['raw'])): ?>
        <script src="assets/js/jquery-1.11.3.min.js"></script>
        <script src="assets/js/form.js"></script>
    <?php
        $form->draw();
    endif;
    ?>
